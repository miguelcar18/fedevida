<?php

/* fedevidaBundle:ventanas:index.html.twig */
class __TwigTemplate_02af064f9134f365a722541ebb5c96b8cc64dc5b8c405e5e0ec08c47b67e18d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
    <title>";
        // line 3
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 4
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        $this->displayBlock('javascripts', $context, $blocks);
        // line 15
        echo "<script type=\"text/javascript\">
\$(document).ready(function(\$){
    \$('#accordion-2').dcAccordion({
            eventType: 'click',
            autoClose: true,
            saveState: false,
            disableLink: true,
            speed: 'fast',
            showCount: false,
            autoExpand: true,
            cookie\t: 'dcjq-accordion-1',
            classExpand\t : 'dcjq-current-parent'
    });\t\t\t\t\t
});
</script>
</head>

<body>
<table width=\"90%\" border=\"0\" align=\"center\" id=\"cabecera\">
      <tr>
        <td><span style=\"font-family:calibri; font-size:16px\">
                <img src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/mint para salud.jpg"), "html", null, true);
        echo "\" width=\"100%\" height=\"74\"/>
            </span></td>
        <td><span style=\"font-family:calibri; font-size:16px\">
                <img src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logo_gobernacion_2013.png"), "html", null, true);
        echo "\" width=\"100%\" height=\"150\">
            </span></td>
      </tr>
       <tr>
    <td align=\"left\" valign=\"middle\" style=\"font-family:calibri; font-size:16px\"><strong>Bienvenido(a): </strong></td>
    <td align=\"right\" valign=\"middle\" style=\"font-family:calibri; font-size:16px\"><strong>Matur&iacute;n, </strong></td>
  </tr>
    </table>
<table width=\"90%\" border=\"0\" align=\"center\">
  <tr>
    <td width=\"26%\" align=\"left\" valign=\"top\">
    <div class=\"graphite demo-container\">
<ul class=\"accordion\" id=\"accordion-2\">
  <li><a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("estatica", array("nombre" => "inicio"));
        echo "\" style=\"border-top: 2px solid #00CC00; border-top-left-radius:10px; border-top-right-radius:10px;\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/inicio_morado.png"), "html", null, true);
        echo "\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;INICIO</a></li>
  <li><a href=\"?sec=ventanas/cambiocontrasena\"><img src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/cambiar_clave.png"), "html", null, true);
        echo "\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;CAMBIAR CLAVE</a></li>
    <li><a href=\"#\"><img src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/carpetas.png"), "html", null, true);
        echo "\" alt=\"\" width=\"24\" height=\"24\" />FE DE VIDA</a>
      <ul>
      <li><a href=\"?sec=ventanas/ingresar_fedevida\">Ingresar</a></li>
            <li><a href=\"?sec=ventanas/listado_personal\">Consultar</a></li>
    </ul>
    </li>
    <li><a href=\"#\"><img src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/reportes.png"), "html", null, true);
        echo "\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;REPORTES</a>
    <ul>
      <li><a href=\"?sec=ventanas/listado_reporte\">Listado General</a></li>
            <li><a href=\"?sec=ventanas/listado_especifico\">Listado Espec&iacute;fico</a></li>
    </ul>
    </li>
    <?php 
\tif(\$tipoa==1)
\t{
\t?>
    <li><a href=\"#\"><img src=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/usuarios.png"), "html", null, true);
        echo "\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;USUARIOS</a><ul>
    <li><a href=\"?sec=ventanas/usuarios/agregar_usuario\">Ingresar nuevo usuario</a></li>
    <li><a href=\"?sec=ventanas/usuarios/lista_modificar_usuario\">Modificar usuarios</a></li>
    <li><a href=\"?sec=ventanas/usuarios/lista_eliminar_usuario\">Eliminar usuarios</a></li>
</ul>
</li>
<li><a href=\"";
        // line 76
        echo $this->env->getExtension('routing')->getPath("autoridades");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/autoridad.png"), "html", null, true);
        echo "\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;AUTORIDADES</a></li>
<li><a href=\"?sec=ventanas/resetear_status\"><img src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/cpanel.png"), "html", null, true);
        echo "\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;RESETEAR STATUS</a></li>
<?php 
}
?>
<li><a href=\"?sec=salir\" style=\"border-bottom-left-radius:10px;border-bottom-right-radius:10px;\"><img src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/cerrar_sesion.png"), "html", null, true);
        echo "\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;CERRAR SESI&Oacute;N</a></li>
</ul>
</div>
    </td>
    <td width=\"74%\" colspan=\"2\" align=\"left\" valign=\"top\">
        ";
        // line 86
        $this->displayBlock('contenido', $context, $blocks);
        // line 87
        echo "    </td>
  </tr>
  <tr>
    <td colspan=\"7\" align=\"center\" valign=\"top\">&nbsp;</td>
  </tr>
  <tr>
    <td colspan=\"7\" align=\"center\" valign=\"top\" style=\"font-family:calibri; font-size:16px\"><marquee><strong>&copy;<?php echo date('Y') ?> Direcci&oacute;n Regional de Salud del Estado Monagas</strong></marquee></td>
  </tr>
</table>
</body>
</html>

";
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Fe de Vida DRS";
    }

    // line 4
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 5
        echo "<link href=\"css/dcaccordion.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"css/estilo_principal.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"css/graphite.css\" rel=\"stylesheet\" type=\"text/css\" />
";
    }

    // line 9
    public function block_javascripts($context, array $blocks = array())
    {
        // line 10
        echo "<script type=\"text/javascript\" src=\"js/jquery.min.js\"></script>
<script type='text/javascript' src='js/jquery.cookie.js'></script>
<script type='text/javascript' src='js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript' src='js/jquery.dcjqaccordion.2.7.min.js'></script>
";
    }

    // line 86
    public function block_contenido($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "fedevidaBundle:ventanas:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 86,  179 => 10,  176 => 9,  169 => 5,  166 => 4,  160 => 3,  144 => 87,  142 => 86,  134 => 81,  127 => 77,  121 => 76,  112 => 70,  99 => 60,  90 => 54,  86 => 53,  80 => 52,  64 => 39,  58 => 36,  35 => 15,  33 => 9,  31 => 4,  27 => 3,  23 => 1,);
    }
}
