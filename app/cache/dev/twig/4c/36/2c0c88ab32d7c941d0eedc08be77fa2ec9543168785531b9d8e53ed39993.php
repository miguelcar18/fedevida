<?php

/* fedevidaBundle:Default:index.html.twig */
class __TwigTemplate_4c362c0c88ab32d7c941d0eedc08be77fa2ec9543168785531b9d8e53ed39993 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
<title>FE DE VIDA</title>

<style>
html { background:#ffffff;
/*background-image: url(images/fondodos.jpg);
background-repeat:no-repeat;
background-size:100% 100%;*/
background: url(images/bg.gif) repeat;
font:Calibri;
   font-family:Calibri;
   font-size:12px }
body
{
   font:Calibri;
   font-family:Calibri;
   font-size:12px
}
fieldset
{
   font:Calibri;
   font-family:Calibri;
}
input
{
   font:Calibri;
   font-family:Calibri;
   font-size:14px
}
#cabecera{
\tbackground-color: #ffffff;
\tborder-left: 2px solid #000000;
\tborder-right: 2px solid #000000;
\tborder-top: 2px solid #000000;
\tborder-bottom: 2px solid #000000;
\tborder-radius: 10px;
\tfont-family:Calibri;
}
</style>

<link href=\"css/dcaccordion.css\" rel=\"stylesheet\" type=\"text/css\" />
<script type=\"text/javascript\" src=\"js/jquery.min.js\"></script>
<script type='text/javascript' src='js/jquery.cookie.js'></script>
<script type='text/javascript' src='js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript' src='js/jquery.dcjqaccordion.2.7.min.js'></script>
<script type=\"text/javascript\">
\$(document).ready(function(\$){
\t\t\t\t\t\$('#accordion-2').dcAccordion({
\t\t\t\t\t\teventType: 'click',
\t\t\t\t\t\tautoClose: true,
\t\t\t\t\t\tsaveState: false,
\t\t\t\t\t\tdisableLink: true,
\t\t\t\t\t\tspeed: 'fast',
\t\t\t\t\t\tshowCount: false,
\t\t\t\t\t\tautoExpand: true,
\t\t\t\t\t\tcookie\t: 'dcjq-accordion-1',
\t\t\t\t\t\tclassExpand\t : 'dcjq-current-parent'
\t\t\t\t\t});
\t\t\t\t\t
\t\t\t\t\t
});
</script>
<link href=\"css/graphite.css\" rel=\"stylesheet\" type=\"text/css\" />

</head>

<body>
<table width=\"90%\" border=\"0\" align=\"center\" id=\"cabecera\">
      <tr>
        <td><span style=\"font-family:calibri; font-size:16px\"><img src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/mint para salud.jpg"), "html", null, true);
        echo "\" width=\"100%\" height=\"74\"/></span></td>
        <td><span style=\"font-family:calibri; font-size:16px\"><img src=\"images/logo_gobernacion_2013.png\" width=\"100%\" height=\"150\"></span></td>
      </tr>
       <tr>
    <td align=\"left\" valign=\"middle\" style=\"font-family:calibri; font-size:16px\"><strong>Bienvenido(a): </strong></td>
    <td align=\"right\" valign=\"middle\" style=\"font-family:calibri; font-size:16px\"><strong>Matur&iacute;n, </strong></td>
  </tr>
    </table>
<table width=\"90%\" border=\"0\" align=\"center\">
  <tr>
    <td width=\"26%\" align=\"left\" valign=\"top\">
    <div class=\"graphite demo-container\">
<ul class=\"accordion\" id=\"accordion-2\">
  <li><a href=\"index.php\" style=\"border-top: 2px solid #00CC00; border-top-left-radius:10px; border-top-right-radius:10px;\"><img src=\"images/inicio_morado.png\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;INICIO</a></li>
  <li><a href=\"?sec=ventanas/cambiocontrasena\"><img src=\"images/cambiar_clave.png\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;CAMBIAR CLAVE</a></li>
    <li><a href=\"#\"><img src=\"images/carpetas.png\" alt=\"\" width=\"24\" height=\"24\" />FE DE VIDA</a>
      <ul>
      <li><a href=\"?sec=ventanas/ingresar_fedevida\">Ingresar</a></li>
            <li><a href=\"?sec=ventanas/listado_personal\">Consultar</a></li>
    </ul>
    </li>
    <li><a href=\"#\"><img src=\"images/reportes.png\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;REPORTES</a>
    <ul>
      <li><a href=\"?sec=ventanas/listado_reporte\">Listado General</a></li>
            <li><a href=\"?sec=ventanas/listado_especifico\">Listado Espec&iacute;fico</a></li>
    </ul>
    </li>
    <?php 
\tif(\$tipoa==1)
\t{
\t?>
    <li><a href=\"#\"><img src=\"images/usuarios.png\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;USUARIOS</a><ul>
    <li><a href=\"?sec=ventanas/usuarios/agregar_usuario\">Ingresar nuevo usuario</a></li>
    <li><a href=\"?sec=ventanas/usuarios/lista_modificar_usuario\">Modificar usuarios</a></li>
    <li><a href=\"?sec=ventanas/usuarios/lista_eliminar_usuario\">Eliminar usuarios</a></li>
</ul>
</li>
<li><a href=\"?sec=ventanas/autoridades\"><img src=\"images/autoridad.png\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;AUTORIDADES</a></li>
<li><a href=\"?sec=ventanas/resetear_status\"><img src=\"images/cpanel.png\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;RESETEAR STATUS</a></li>
<?php 
}
?>
<li><a href=\"?sec=salir\" style=\"border-bottom-left-radius:10px;border-bottom-right-radius:10px;\"><img src=\"images/cerrar_sesion.png\" alt=\"\" width=\"24\" height=\"24\" />&nbsp;CERRAR SESI&Oacute;N</a></li>
</ul>
</div>
    </td>
    <td width=\"74%\" colspan=\"2\" align=\"left\" valign=\"top\"></td>
  </tr>
  <tr>
    <td colspan=\"7\" align=\"center\" valign=\"top\">&nbsp;</td>
  </tr>
  <tr>
    <td colspan=\"7\" align=\"center\" valign=\"top\" style=\"font-family:calibri; font-size:16px\"><marquee><strong>&copy;<?php echo date('Y') ?> Direcci&oacute;n Regional de Salud del Estado Monagas</strong></marquee></td>
  </tr>
</table>
</body>
</html>

";
    }

    public function getTemplateName()
    {
        return "fedevidaBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 55,  132 => 51,  128 => 49,  107 => 36,  61 => 13,  273 => 96,  269 => 94,  254 => 92,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 61,  143 => 56,  135 => 53,  119 => 42,  102 => 32,  71 => 19,  67 => 15,  63 => 15,  59 => 14,  38 => 6,  94 => 28,  89 => 20,  85 => 25,  75 => 17,  68 => 14,  56 => 9,  87 => 25,  21 => 2,  26 => 6,  93 => 28,  88 => 6,  78 => 21,  46 => 7,  27 => 4,  44 => 12,  31 => 5,  28 => 3,  201 => 92,  196 => 90,  183 => 82,  171 => 61,  166 => 71,  163 => 62,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 54,  136 => 56,  121 => 46,  117 => 44,  105 => 40,  91 => 71,  62 => 23,  49 => 19,  24 => 4,  25 => 3,  19 => 1,  79 => 18,  72 => 16,  69 => 25,  47 => 9,  40 => 8,  37 => 10,  22 => 2,  246 => 90,  157 => 56,  145 => 46,  139 => 45,  131 => 52,  123 => 47,  120 => 40,  115 => 43,  111 => 37,  108 => 36,  101 => 32,  98 => 31,  96 => 31,  83 => 25,  74 => 14,  66 => 15,  55 => 15,  52 => 21,  50 => 10,  43 => 8,  41 => 7,  35 => 5,  32 => 4,  29 => 3,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 72,  164 => 59,  162 => 57,  154 => 58,  149 => 51,  147 => 58,  144 => 49,  141 => 48,  133 => 55,  130 => 41,  125 => 44,  122 => 43,  116 => 41,  112 => 42,  109 => 34,  106 => 36,  103 => 32,  99 => 31,  95 => 28,  92 => 21,  86 => 28,  82 => 22,  80 => 19,  73 => 19,  64 => 17,  60 => 6,  57 => 11,  54 => 10,  51 => 14,  48 => 13,  45 => 17,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 7,);
    }
}
