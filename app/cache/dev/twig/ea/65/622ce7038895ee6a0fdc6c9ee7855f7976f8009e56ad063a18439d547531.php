<?php

/* fedevidaBundle:ventanas:inicio.html.twig */
class __TwigTemplate_ea65622ce7038895ee6a0fdc6c9ee7855f7976f8009e56ad063a18439d547531 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("fedevidaBundle:ventanas:index.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "fedevidaBundle:ventanas:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "Inicio";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 6
        echo "<link href=\"../../css/dcaccordion.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"../../css/estilo_principal.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"../../css/graphite.css\" rel=\"stylesheet\" type=\"text/css\" />
";
    }

    // line 10
    public function block_javascripts($context, array $blocks = array())
    {
        // line 11
        echo "<script type=\"text/javascript\" src=\"../../js/jquery.min.js\"></script>
<script type='text/javascript' src='../../js/jquery.cookie.js'></script>
<script type='text/javascript' src='../../js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript' src='../../js/jquery.dcjqaccordion.2.7.min.js'></script>
";
    }

    // line 16
    public function block_contenido($context, array $blocks = array())
    {
        // line 17
        echo "<table width=\"100%\" border=\"0\" align=\"center\">
    <tr>
        <td colspan=\"2\" bgcolor=\"#CCCCCC\" class=\"TITULO\" align=\"center\" style=\"border-top-left-radius:10px; border-top-right-radius:10px;\">BIENVENIDOS AL CENSO REGIONAL DE FE DE VIDA MONAGAS <?php echo date('Y');?></td>
    </tr>
    <tr>
        <td align=\"center\" colspan=\"2\"><img src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/gestion1.png"), "html", null, true);
        echo "\" width=\"221\" height=\"181\" /></td>
    </tr>
    <tr>
        <td colspan=\"2\" bgcolor=\"#CCCCCC\" class=\"TITULO\" align=\"center\" style=\"border-bottom-left-radius:10px; border-bottom-right-radius:10px;\">SOLICITE TODOS LOS REQUISITOS AL BENEFICIADO. VERIFIQUE E INGRESE</td>
    </tr>
</table>
";
    }

    public function getTemplateName()
    {
        return "fedevidaBundle:ventanas:inicio.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 22,  61 => 17,  58 => 16,  50 => 11,  47 => 10,  40 => 6,  37 => 5,  31 => 4,);
    }
}
