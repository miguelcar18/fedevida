<?php

/* fedevidaBundle:ventanas:autoridades.html.twig */
class __TwigTemplate_3602a8348254d4b8ff47c28896c693b49a282db26e08fc7e4e7904cb7512f3e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("fedevidaBundle:ventanas:index.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "fedevidaBundle:ventanas:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "Autoridades";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 6
        echo "<link href=\"../css/dcaccordion.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"../css/estilo_principal.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"../css/graphite.css\" rel=\"stylesheet\" type=\"text/css\" />
<style>
#tablaautoridades
{
border: 1px solid black;
font-family:\"Calibri\";
}
\t</style>
";
    }

    // line 17
    public function block_javascripts($context, array $blocks = array())
    {
        // line 18
        echo "<script type=\"text/javascript\" src=\"../js/jquery.min.js\"></script>
<script type='text/javascript' src='../js/jquery.cookie.js'></script>
<script type='text/javascript' src='../js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript' src='../js/jquery.dcjqaccordion.2.7.min.js'></script>
";
    }

    // line 23
    public function block_contenido($context, array $blocks = array())
    {
        // line 24
        echo "<div class=\"art-post-inner art-article\" style=\"height:400px; border:none\" align=\"center\">

<br>

<table width=\"600\" align=\"center\" id=\"tablaautoridades\">
  <tr align=\"center\" bgcolor=\"#00CC00\" style=\"color:#FFF; font-family:'Calibri';\">
    <td width=\"95\" style=\"vertical-align:middle; text-align:center\"><strong>MODIFICAR</strong></td>
    <td width=\"327\" style=\"vertical-align:middle; text-align:center\"><strong>NOMBRE</strong></td>
    <td width=\"156\" style=\"vertical-align:middle; text-align:center\"><strong>CARGO</strong></td>
  </tr>
  
  ";
        // line 35
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["autoridades"]) ? $context["autoridades"] : $this->getContext($context, "autoridades")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["autoridad"]) {
            // line 36
            echo "  
  <tr style=\"font-family:'Calibri'\">
    <td align=\"center\" bgcolor=\"#FFFFFF\" style=\"vertical-align:middle; text-align:center\">
        <a href=\"#\" title=\"MODIFICAR NOMBRE\">
           <img src=\"images/lapiz.png\" alt=\"\" width=\"24\" height=\"24\" />
        </a>
    </td>
    <td bgcolor=\"#FFFFFF\" style=\"color:#000; vertical-align:middle; text-align:left\">";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["autoridad"]) ? $context["autoridad"] : $this->getContext($context, "autoridad")), "nombreAutoridad"), "html", null, true);
            echo "</td>
    <td bgcolor=\"#FFFFFF\" style=\"color:#000; vertical-align:middle; text-align:left\">";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["autoridad"]) ? $context["autoridad"] : $this->getContext($context, "autoridad")), "cargoAutoridad"), "html", null, true);
            echo "</td>
  </tr>
  ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 47
            echo "  <td colspan=\"3\">No hay autoridades</td>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['autoridad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "</table>


</div>
";
    }

    public function getTemplateName()
    {
        return "fedevidaBundle:ventanas:autoridades.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 49,  107 => 47,  99 => 44,  95 => 43,  86 => 36,  81 => 35,  68 => 24,  65 => 23,  57 => 18,  54 => 17,  40 => 6,  37 => 5,  31 => 4,);
    }
}
