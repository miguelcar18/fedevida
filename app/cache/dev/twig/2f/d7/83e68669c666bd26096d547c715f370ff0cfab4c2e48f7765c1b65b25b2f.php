<?php

/* fedevidaBundle:ventanas:registro.html.twig */
class __TwigTemplate_2fd783e68669c666bd26096d547c715f370ff0cfab4c2e48f7765c1b65b25b2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("fedevidaBundle:ventanas:index.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "fedevidaBundle:ventanas:index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "Registro Autoridades";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 6
        echo "<link href=\"../css/dcaccordion.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"../css/estilo_principal.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"../css/graphite.css\" rel=\"stylesheet\" type=\"text/css\" />
";
    }

    // line 10
    public function block_javascripts($context, array $blocks = array())
    {
        // line 11
        echo "<script type=\"text/javascript\" src=\"../js/jquery.min.js\"></script>
<script type='text/javascript' src='../js/jquery.cookie.js'></script>
<script type='text/javascript' src='../js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript' src='../js/jquery.dcjqaccordion.2.7.min.js'></script>
";
    }

    // line 16
    public function block_contenido($context, array $blocks = array())
    {
        // line 17
        echo "<h1>FORMULARIO</h1>

";
        // line 19
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "

";
    }

    public function getTemplateName()
    {
        return "fedevidaBundle:ventanas:registro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 19,  61 => 17,  58 => 16,  50 => 11,  47 => 10,  40 => 6,  37 => 5,  31 => 4,);
    }
}
