<?php

namespace fedevida\fedevidaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * autoridades
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="fedevida\fedevidaBundle\Entity\autoridadesRepository")
 */
class autoridades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_autoridad", type="string", length=100)
     */
    private $nombreAutoridad;

    /**
     * @var string
     *
     * @ORM\Column(name="cargo_autoridad", type="string", length=100)
     */
    private $cargoAutoridad;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreAutoridad
     *
     * @param string $nombreAutoridad
     * @return autoridades
     */
    public function setNombreAutoridad($nombreAutoridad)
    {
        $this->nombreAutoridad = $nombreAutoridad;

        return $this;
    }

    /**
     * Get nombreAutoridad
     *
     * @return string 
     */
    public function getNombreAutoridad()
    {
        return $this->nombreAutoridad;
    }

    /**
     * Set cargoAutoridad
     *
     * @param string $cargoAutoridad
     * @return autoridades
     */
    public function setCargoAutoridad($cargoAutoridad)
    {
        $this->cargoAutoridad = $cargoAutoridad;

        return $this;
    }

    /**
     * Get cargoAutoridad
     *
     * @return string 
     */
    public function getCargoAutoridad()
    {
        return $this->cargoAutoridad;
    }
}
