<?php

namespace fedevida\fedevidaBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UsuarioType extends AbstractType{
    public function buildForm(FormBuilder $builder){
        
        $builder->add('nombre', 'text', array())
                ->add('cargo');
    }

    

    public function getName() {
        
    }

}