<?php

namespace fedevida\fedevidaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use fedevida\fedevidaBundle\Entity\autoridades;
//use fedevida\fedevidaBundle\Form\UsuarioType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        //return $this->render('fedevidaBundle:Default:index.html.twig', array('name' => $name));
        
        return $this->render('fedevidaBundle:ventanas:index.html.twig');
    }
    public function estaticaAction($nombre)
    {
        
        $autoridades = new autoridades();
        $autoridades->setNombreAutoridad('Miguel Carmona');
        $autoridades->setCargoAutoridad('Programador');

        $em = $this->getDoctrine()->getManager();
        $em->persist($autoridades);
        $em->flush();
        return $this->render('fedevidaBundle:ventanas:'.$nombre.'.html.twig');
    }
    public function principalAction()
    {
        return $this->render('fedevidaBundle:ventanas:index.html.twig');
    }
    public function autoridadesAction()
    {
        $repository=$this->getDoctrine()->getRepository("fedevidaBundle:autoridades");
        $autoridades=$repository->findAll();
        return $this->render('fedevidaBundle:ventanas:autoridades.html.twig', array('autoridades' => $autoridades));
    }
    
    public function registroAction()
    {/*
        $form = $this->get('form.factory')->create(
                new UsuarioType(),array()
                );*/
        
        $autoridades = new autoridades();
 
        $form = $this->createFormBuilder($autoridades)
            ->add('nombreAutoridad')
            ->add('cargoAutoridad')
            ->getForm();
        return $this->render('fedevidaBundle:ventanas:registro.html.twig',
                array('form'=>$form->createView())
                );
    }
}
